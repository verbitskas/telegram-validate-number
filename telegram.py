#coding: utf-8

__AUTHOR__ = 'Antanas Verbitskas'
__EMAIL__  = 'johnmnemonik.jm@gmail.com'

import asyncio
import argparse
import time
import logging
import os
import sys

from telethon.sync import TelegramClient, types, functions
import aiofiles
import socks


logging.basicConfig(
	#filename='log.txt',
	format="%(levelname)-10s строка %(lineno)d %(message)s",
	level=logging.INFO
)

DEBUG = False

class CapitalisedHelpFormatter(argparse.HelpFormatter):
    def add_usage(self, usage, actions, groups, prefix=None):
        if prefix is None:
            prefix = 'Использование: '

        return super(CapitalisedHelpFormatter, self).add_usage(
            usage, actions, groups, prefix)



def _exception_handler(loop, context):
	# если раскоментироать поваляться ошибки
	# loop.default_exception_handler(context)
	exception = context.get('exception')

	if isinstance(exception, Exception):
		logging.exception(context)
		logging.info("перехватил")


class Crawler:
	def __init__(self, input_txt, output_txt, folder, count, timeout, loop=None):
		self.loop = loop or asyncio.get_event_loop()
		self.queue_number = asyncio.Queue()
		self.queue_account = asyncio.Queue()
		self.lock = asyncio.Lock()
		self.input_txt = input_txt
		self.output_txt = output_txt
		self.folder = folder
		self.sem = asyncio.BoundedSemaphore(count)
		self.timeout = timeout
		self.num = 4
		
		_ = []
		try:
			with open(self.input_txt, 'r') as f:
				for i in f:
					_.append(i)
				for number_phone in _:
					if number_phone.strip() != '':
						self.queue_number.put_nowait(number_phone.strip())
		except FileNotFoundError as exc:
			sys.exit("нет такого файла --> {}".format(self.input_txt))
		except Exception as exc:
			logging.exception(exc)

		self.client = None

		async def account_build(account="accounts.txt"):
			logging.info("сборка аккаунтов")
			with open(self.folder + '/' + account, 'rt') as f:
				for i in f:
					dic = {}
					_ = i.strip().split(' ')
					dic['api_id'] = int(_[0])
					dic['api_hash'] = _[1]
					dic['username'] = _[2]
					await self.queue_account.put(dic)

		self.loop.run_until_complete(account_build())
		self.loop.run_until_complete(self._do_connect())
		logging.info("НОМЕРОВ НА ПРОВЕРКУ: %s", self.queue_number.qsize())



	async def save(self, username, number_phone):
		# исправим чуть позже
		async with aiofiles.open(self.output_txt, mode='a') as f:
			await f.write("{}:{}\n".format(username, number_phone))
			await f.close()



	async def _connect_build(self):
		if not self.queue_account.empty():
			dic = await self.queue_account.get()
			await self.queue_account.put(dic)
			return (int(dic['api_id']), dic['api_hash'], dic['username'])
		else:
			logging.info("АККАУНТЫ КОНЧИЛИСЬ")


	async def _do_connect(self):

		if self.client is not None and self.client.is_connected():
			logging.info("ПОДКЛЮЧЕН, МЕНЯЕМ")
			await self.client.disconnect()


		api_id, api_hash, username = await self._connect_build()
		self.client = TelegramClient(username, api_id, api_hash, proxy=(socks.SOCKS5, '127.0.0.1', 9050))
		logging.info("РЕСТАРТ С ПАРАМЕТРАМИ %s %s %s", api_id, api_hash, username)
		await self.client.start()


	async def check(self):
		while True:
			if self.num <= 4:
				try:
					async with self.sem:
						number_phone = await self.queue_number.get()
						contact = types.InputPhoneContact(
							client_id=0,
							phone=number_phone,
							first_name="custom_first_name",
							last_name="custom_last_name")
						result = await self.client(functions.contacts.ImportContactsRequest([contact]))
						contact_info = await self.client.get_entity(number_phone)
						if contact_info.username is not None:
							logging.info(contact_info.username)
							await self.save(contact_info.username, number_phone)
						async with self.lock:
							self.num -= 1
							break
						logging.info("НОМЕРОВ ОСТАЛОСЬ: %s", self.queue_number.qsize())
				except Exception:
					async with self.lock:
						self.num -= 1
						logging.info("НОМЕРОВ ОСТАЛОСЬ: %s", self.queue_number.qsize())
						break
			else:
				logging.info("в check кончились num --> %s", self.num)
				break

		# TODO
		async with self.lock:
			logging.info("СРАБОТАЛА СМЕНА АККА, колличество %s", self.num)
			if self.num <= 0:
				await self._do_connect()
			else:
				logging.info("НЕ СМЕНИЛИ АКК NUM == %s", self.num)



	async def _bootstrap(self):
		tasks = [asyncio.Task(self.check()) for _ in range(self.queue_number.qsize())]
		for task in asyncio.as_completed(tasks):
			try:
				res = await task
			except asyncio.CancelledError:
				pass


	def run(self):
		loop = asyncio.get_event_loop()
		loop.set_exception_handler(_exception_handler)

		if DEBUG:
			loop.set_debug(True)
			loop.slow_callback_duration = 0.003

		try:
			loop.run_until_complete(self._bootstrap())
		except asyncio.TimeoutError:
			logging.info('время вышло')
		finally:
			pass





def do_start():
	arg_parser = argparse.ArgumentParser(
		description='чекер telegram аккаунтов',
		formatter_class=CapitalisedHelpFormatter,
		add_help=False)
	arg_parser.add_argument('-i', '--input', default='number.txt', help='файл с номерами', type=str)
	arg_parser.add_argument('-o', '--output', default='good.txt', help='файл с результатами', type=str)
	arg_parser.add_argument('-f', '--folder', help='папка с аккаунтами', type=str)

	arg_parser.add_argument('-c', '--count', default=4, type=int, help='колличество потоков')
	arg_parser.add_argument('-t', '--timeout', default=30, type=int, help='Таймаут в секундах')


	arg_parser.add_argument('-h', '--help', action='help', default=argparse.SUPPRESS,
		help='Показать это справочное сообщение и выйти.')

	arg_parser._positionals.title = 'Позиционные аргументы'
	arg_parser._optionals.title = 'Аргументы'
	args = arg_parser.parse_args()
	return (args.input, args.output, args.folder, args.count, args.timeout)


def main():
	input_txt, output_txt, folder, count, timeout = do_start()
	logging.info("семафор %s", count)
	crawler = Crawler(input_txt, output_txt, folder, count, timeout)
	crawler.run()



if __name__ == '__main__':
	main()